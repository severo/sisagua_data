#!/bin/bash

input_dir="./input/original/"
input_extension="xlsx"
output_dir="./cached/csv_from_original/"

# Required:
# - bash

# Convert to CSV
function convert_xlsx_to_csv {
  mkdir -p ${output_dir}
  rm -f "${output_dir}*.csv"
  libreoffice --headless --convert-to csv ${input_dir}*.${input_extension} --outdir $output_dir
  recode iso88591..utf8 ${output_dir}*.csv
}

convert_xlsx_to_csv
