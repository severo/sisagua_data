# SISAGUA data

Data from SISAGUA on drinking water quality (concentration of pesticides) in Brazil. Provided by PublicEye.

## Create cached data

XLSX files take time to load in R. It's quicker to load CSV files. To prepare the CSV files, launch:

```bash
./xlsx_to_csv.sh
```

and see the result in the `cached/csv_from_original/` directory.
